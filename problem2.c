#include<stdio.h>

int TriangularMatrix(int n, int (*arr)[n]){
  int up_matrix = 0,down_matrix = 0,check=0,chk_till=n+1;
  for(int i=0;i<(n*n);i++){
      check = i%(n+1);
      if(check==0){
        chk_till--;
        //printf("%d\n",chk_till);
      }
      else{
        //printf("Check :%d\n",check);
        if(check<chk_till){
            if(*(*arr+i)==0){
              down_matrix++;
              //printf("DOWN!\n");
            }
        }
        else{
          if(*(*arr+i)==0){
              up_matrix++;
              //printf("UP!\n");
            }
        }
      }
    }

  if(up_matrix == n*(n-1)/2 && down_matrix == n*(n-1)/2)
    return 3;
  else if(up_matrix == n*(n-1)/2)
    return 2;
  else if(down_matrix == n*(n-1)/2) 
    return 1;
  else 
    return 0;
}


int main() {
  int n;
  printf("Enter the size of matrix : ");
  scanf(" %d",&n);
  int arr[n][n];
  printf("Enter the array elements :\n");
  for(int i=0;i<(n*n);i++){
    printf("Element %d:",i+1);
    scanf(" %d",*arr+i);
  }
  switch(TriangularMatrix(n,arr)){
    case 3:
      printf("\nDiagonal Matrix\n");break;
    case 2:
      printf("\nUpper Triangular matrix\n");break;
    case 1:
      printf("\nLower Triangular matrix\n");break;
    case 0:
      printf("\nJARVIM (Just A Rather Very Interesting Matrix)\n");break;
  }
  return 0;
}
