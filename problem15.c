#include<stdio.h>
#include<stdlib.h>
#define CURR_YEAR 2018

typedef struct{
    int accno;
    char title[30];
    int price;
    struct{
        int month;
        int year;
    }month_year_publication;
}book;

book * search_book(int total,int search,book *books){
    for(int i=0;i<total;i++){
        if(books[i].accno == search){
            return &books[i];
        }
    }
}

void get_details(book *books,int i){
    printf("Enter account number       : ");
    scanf("%d",&books[i].accno);
    printf("Enter the title            : ");
    scanf("%s",books[i].title);
    printf("Enter price                : ");
    scanf("%d",&books[i].price);
    printf("Enter month of publication : ");
    scanf("%d",&books[i].month_year_publication.month);
    printf("Enter year of publication  : ");
    scanf("%d",&books[i].month_year_publication.year);
}

void print_details(book *books,int i){
    printf("Account number       : %d\n",books[i].accno);
    printf("Title                : %s\n",books[i].title);
    printf("Price                : %d\n",books[i].price);
    printf("Month of publication : %d\n",books[i].month_year_publication.month);
    printf("Year of publication  : %d\n",books[i].month_year_publication.year);
}

int main() {
    int n;
    printf("Enter the number of books : ");
    scanf("%d",&n);
    printf("\n");
    book *books = (book *) malloc(n * sizeof(book));
    for(int i=0;i<n;i++){
        printf("Book %d : \n",i+1);
        get_details(books,i);
    }

    int search; 
    printf("\nEnter the account number of the book to search for : ");
    scanf("%d",&search);
    book *searched = search_book(n,search,books);
    print_details(searched,0);
    
    printf("\nBooks of current year : \n");
    for(int i=0;i<n;i++){
        if(books[i].month_year_publication.year == CURR_YEAR){
            print_details(books,i);
            printf("\n");
        }
    }
    return 0;
}