#include<stdio.h>

int main() {
    int n;
    printf("Enter the size of array : ");
    scanf("%d",&n);
    while(n>0){
        int arr[n];
        printf("Enter array elements :\n");
        for(int *i=arr;i<arr+n;i++){
            printf("Element %d : ",i-arr+1);
            scanf("%d",i);
        }
        for(int *i=arr;i<arr+n-1;i++){
            *(i+1) += *i;
        }
        printf("Sum of elements in the array is : %d\n",*(arr+n-1));
    }
    printf("Program exited succesfully\n");
    return 0;
}