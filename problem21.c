#include<stdio.h>

unsigned long long series_sum(unsigned int n){
    unsigned long long sum=0,prod=1;
    for(int i=1;i<=n;i++){
        for(int j=1;j<i;j++){
            prod *=j;
        }
        sum += prod;
        prod=1;
    }
    return sum;
}

int main() {
    unsigned int n;
    printf("Enter the number till the series sum is calculated : ");
    scanf("%u",&n);
    printf("Sum of the series is : %llu\n",series_sum(n));
    return 0;
}