#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#define CURRENT_DATE 20180614
struct drugs{
  int drug_code;
  char drug_name[25];
  char exp_date[10];
  int count;
}stock[100]={},expd[100]={};

void read_drugs(int n){
  struct drugs tempdrug;
  int stkc=0,expc=0;
  for(int i=0;i<n;i++){
    printf("Enter drug code     : ");
    scanf("%d",&tempdrug.drug_code);
    printf("Enter drug name     : ");
    scanf(" %s",tempdrug.drug_name);
    printf("Enter expiry date   : ");
    scanf(" %s",tempdrug.exp_date);
    printf("Enter count of drug : ");
    scanf("%d",&tempdrug.count);
  }
  long tempdate =  (tempdrug.exp_date[0]-48)*pow(10,1)+(tempdrug.exp_date[1]-48)*pow(10,0)+
                   (tempdrug.exp_date[3]-48)*pow(10,3)+(tempdrug.exp_date[4]-48)*pow(10,2)+
                   (tempdrug.exp_date[6]-48)*pow(10,7)+(tempdrug.exp_date[7]-48)*pow(10,6)+
                   (tempdrug.exp_date[8]-48)*pow(10,5)+(tempdrug.exp_date[9]-48)*pow(10,4);
  if(tempdate >= CURRENT_DATE){
    stock[stkc] = tempdrug;
    stkc++;
  }
  else{
    expd[expc] = tempdrug;
    expc++;
  }
}

int exp_drugs(struct drugs stock[],struct drugs expd[],int n);

int main() {
  int n;
  printf("Enter the no.of drugs : ");
  scanf("%d",&n);
  read_drugs(n);
  printf("No.of drugs expired are : %d\n",exp_drugs(stock,expd,n));
  return 0;
}

int exp_drugs(struct drugs stock[],struct drugs expd[],int n){
  int sum = 0;
  for(int i=0;i<n;i++){
    sum += expd[i].count;
  }
  return sum;
}
