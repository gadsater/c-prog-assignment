#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<limits.h>
#include<math.h>

char *strrev(char *str)
{
      char *p1, *p2;

      if (! str || ! *str)
            return str;
      for (p1 = str, p2 = str + strlen(str) - 1; p2 > p1; ++p1, --p2)
      {
            *p1 ^= *p2;
            *p2 ^= *p1;
            *p1 ^= *p2;
      }
      return str;
}

char * binstring(int n){
    char *bin = (char *) malloc(33*sizeof(char));
    memset(bin,'0',33);
    int size = 1;
    int i=2;
    if (n<0) bin[31]='1';
    while(abs(n)>0){
        bin[size-1]=48+abs(n)%i;
        n=n/i;
        size++;
    }
    bin[32] ='\0';
    bin = strrev(bin);
    return bin;

}

int main(){
    int n;
    printf("Enter the number : ");
    scanf("%d",&n);
    printf("Binary number    : %s\n",binstring(n));
    return 0;
}