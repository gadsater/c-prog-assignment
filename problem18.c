#include<stdio.h>

void matrix_multiplication(int m, int n, int (*arr)[n],int temp[m][m]) {
    int sum =0;
    for(int i=0;i<m;i++){
        for(int j=0;j<m;j++){
            for(int k=0;k<n;k++){
            sum += *(*(arr+i)+k) * *(*(arr+j)+k);
            }
            temp[i][j] = sum;
            sum = 0;
        }
    }
}


int main() {
    int m=0,n=0;
    printf("Enter the dimensions of the matrix(MxN) : ");
    scanf("%d%*c%d",&m,&n);
    int arr[m][n];
    int trans[n][m];
    int temp[m][m];

    printf("Enter array elements :\n");
    for(int i=0;i<m;i++){
        for(int j=0;j<n;j++){
            scanf("%d",&arr[i][j]);
            trans[j][i] = arr[i][j];
        }
    }

    printf("Tranpose of the array is :\n");
    for(int i=0;i<n;i++){
        for(int j=0;j<m;j++){
            printf("% 5d ",trans[i][j]);
        }
        printf("\n");
    }
    
    matrix_multiplication(m,n,arr,temp);

    printf("Product of transpose and the given array is : \n");
    for(int i=0;i<m;i++){
        for(int j=0;j<m;j++){
            printf("% 5d ",temp[i][j]);
        }
        printf("\n");
    }

    return 0;
}