#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#define TOKEN_STR(x) #x

typedef struct{
    char name[30];
    char street[50];
    char city[30];
    unsigned int acc_number;
    char acc_type[30];
    int prev_bal;
    int curr_pay;
    int new_bal;
    char pay_date[10];
}bill_system;

int int_input(char s[]){
    int n;
    printf("%s",s);
    scanf("%d",&n);
    return n;
}

void str_input(char arr[],char s[]){
    printf("%s",s);
    scanf("%s",arr);
}

void read_details(int n, bill_system cust_details[n]){
    for(int i=0;i<n;i++){
        printf("\n");
        str_input(cust_details[i].name,"Enter the name         : ");
        str_input(cust_details[i].street,"Enter street name      : ");
        str_input(cust_details[i].city,"Enter city name        : ");
        cust_details[i].acc_number = int_input("Enter account number   : ");
        cust_details[i].prev_bal   = int_input("Enter previous balance : ");
        cust_details[i].curr_pay   = int_input("Enter current payment  : ");
        str_input(cust_details[i].pay_date,"Enter pay date         : ");
    }
}

void update_details(int n,bill_system cust_details[n]){
    for(int i=0;i<n;i++){
        if(cust_details[i].curr_pay >0 && cust_details[i].curr_pay < 0.1*cust_details[i].prev_bal){
            strcpy(cust_details[i].acc_type,"overdue");
        }
        else if(cust_details[i].prev_bal>0 && cust_details[i].curr_pay==0){
            strcpy(cust_details[i].acc_type,"delinquent");
        }
        else{
            strcpy(cust_details[i].acc_type,"current");
        }
        cust_details[i].new_bal = cust_details[i].prev_bal - cust_details[i].curr_pay;
    }
}

void print_details(int n, bill_system cust_details[n]){
    for(int i=0;i<n;i++){
        printf("\nCustomer %d :\n",i+1);
        printf("Customer name    : %s\n",cust_details[i].name);
        printf("Customer street  : %s\n",cust_details[i].street);
        printf("Customer city    : %s\n",cust_details[i].city);
        printf("Account number   : %d\n",cust_details[i].acc_number);
        printf("Account type     : %s\n",cust_details[i].acc_type);
        printf("Previous balance : %d\n",cust_details[i].prev_bal);
        printf("Current payment  : %d\n",cust_details[i].curr_pay);
        printf("New Balance      : %d\n",cust_details[i].new_bal);
        printf("Payment date     : %s\n",cust_details[i].pay_date);
    }
}

int main() {
    int n;
    printf("WELCOME TO THE BILLING SYSTEM\n");
    n=int_input("Enter the number of customers : ");
    bill_system cust_details[n];
    read_details(n,cust_details);
    update_details(n, cust_details);
    print_details(n,cust_details);
    return 0;
}