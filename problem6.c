#include<stdio.h>
#include<string.h>

typedef struct {
    double a;
    double b;
}comp_num;

comp_num sumComplexNumbers(comp_num m,comp_num n){
    comp_num l;
    l.a = m.a + n.a;
    l.b = m.b + n.b;
    return l;
}


int main() {
    comp_num m,n,r;
    printf("Enter the first complex number  : ");
    scanf("%lf%*c%lf%*c",&m.a,&m.b);
    printf("Enter the second complex number : ");
    scanf("%lf%*c%lf%*c",&n.a,&n.b);
    r = sumComplexNumbers(m,n);
    printf("Sum of the complex numbers is   : %g+%gi\n",r.a,r.b);
    return 0;
}