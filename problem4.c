#include<stdio.h>

double power(double m,int n) {
    double product=1;
    if (n==0) return m*m;
    else{
        for(int i=0;i<n;i++){
            product*=m;
        }
        return product;
    }
}

int main(){
    double m;
    int n;
    printf("Enter the value of m and n : ");
    scanf(" %lf %d",&m,&n);

    printf("The calculated power is (defaults to square) : %lf\n",power(m,n));
    return 0;
}