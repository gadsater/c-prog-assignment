#include<stdio.h>
#include<string.h>
#define MAX_EMPLOYEE 100

struct employee {
  int emp_id;
  char name[30];
  float sal;
  char dept[30];
}emp[MAX_EMPLOYEE]={};

int add_emp(){
  static int count = 0;
  char temp[30];
  printf("Enter Employee ID         : ");
  scanf("%d",&emp[count].emp_id);
  printf("Enter Employee Name       : ");
  scanf("%s",temp);
  strcpy(emp[count].name,temp);
  printf("Enter Salary              : ");
  scanf("%f",&emp[count].sal);
  printf("Enter Employee Department : ");
  scanf("%s",temp);
  strcpy(emp[count].dept,temp);
  count++;
  printf("\nEmployee Successfully added");
  return count;
}

void print_emp(int t) {
  printf("Employee %d's Details : \n\n",t);
  printf("Employee ID         : %d\n",emp[t-1].emp_id);
  printf("Employee Name       : %s\n",emp[t-1].name);
  printf("Salary              : %f\n",emp[t-1].sal);
  printf("Employee Department : %s\n",emp[t-1].dept);
}

void sal_raise(){
  int temp;
  printf("Enter employee ID : ");
  scanf("%d",&temp);

  for(int i=0;i<MAX_EMPLOYEE;i++){
    if(emp[i].emp_id==temp){
      emp[i].sal= emp[i].sal*1.1;
      break;
    }
  }
  printf("\nEmployee does not exist!");
}
int main() {
  int menu,count=0;
  char ans;
  printf("*** Program to deal with employee details *** \n\n\n");
  do {
    printf("-- Employee Menu --\n\n");
    printf("1.Add Employee\n");
    printf("2.Print Employee details\n");
    printf("3.Increase Salary\n\n");

    printf("Choose a menu : ");
    scanf("%d",&menu);
    printf("\n\n");

    switch(menu) {
      case 1:
        count = add_emp();
        break;
      case 2:{
        block1:
        printf("Enter employee no : ");
        scanf("%d",&menu);
        printf("\n");
        if(menu<=count && menu!=0){
        print_emp(menu);
        }
        else{
          printf("There is/are only %d employee(s).\n",count);
          if(count==0) break;
          goto block1;
        }
        break;
      }
      case 3:
        sal_raise();
        break;
      default:
        printf("Enter A Valid option(1-3)");
    }

    printf("\n\n Do you wish to continue(y/n) ? ");
    scanf(" %c",&ans);
  }while(ans=='y');
  return 0;
}
