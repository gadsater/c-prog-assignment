#include<stdio.h>
#include<stdlib.h>

void add_elem_array(char *arr, int *size, char element, int position){
    (*size)++;
    arr = (char *) realloc(arr,*size * sizeof(int));
    for(int i=(*size)-1;i>=position-1;i--){
        arr[i+1] = arr[i];
    }
    arr[position-1] = element;
}

int main() {
    int size;
    printf("Enter the size of array : ");
    scanf("%d",&size);
    char *arr = (char *) malloc(size*sizeof(char));
    printf("Enter array elements : ");
    for(int i=0;i<size;i++){
        scanf(" %c",&arr[i]);
    }
    char ans,element;
    int position;
    repeat:
    printf("Would you like to add one more element into the array(y|n) : ");
    scanf(" %c",&ans);
    if(ans=='y'){
        printf("Enter the element             : ");
        scanf(" %c",&element);
        printf("Enter its position(1st index) : ");
        scanf("%d",&position);
        add_elem_array(arr,&size,element,position);
        printf("Array elements are            : ");
        for(int i=0;i<size;i++){
            printf("%c ",arr[i]);
        }
        printf("\n");
        goto repeat;
    }
    return 0;
}