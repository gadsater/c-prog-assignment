#include<stdio.h>

unsigned int digitsum(unsigned int n){
    return n==0 ? 0 : n%10 + digitsum(n/10);
}

int main() {
    unsigned int n;
    printf("Program to find sum of digits \n");
    printf("Enter the number      : ");
    scanf("%u",&n);
    printf("Sum of the digits are : %u\n",digitsum(n));
    return 0;
}