#include<stdio.h>

void strrev(char *str,int len){
    if(len){
        printf("%c",*(str+len-1));
        strrev(str,len-1);
    }
}

int my_strlen(char *str){
    unsigned int count = 0;
    while(*str){
        count++;
        str++;
    }
    return count;
}

int main() {
    char str[100];
    printf("Enter a string : ");
    fgets(str,100,stdin);
    int string_length = my_strlen(str);
    printf("The string after reversal is : ");
    strrev(str,string_length);
    printf("\n");
    return 0;

}