#include<stdio.h>
#include<math.h>

void lcm_gcd(int, int, int*, int*);
int min(int, int);
int abs(int);

int main() {
    int a,b,lcm,gcd;
    printf("Enter two numbers : ");
    scanf("%d %d",&a,&b);
    lcm_gcd(a,b,&gcd,&lcm);
    printf("GCD : %d\n",gcd);
    printf("LCM : %d\n",lcm);
    return 0;
}

int min(int a,int b){
    return a<b?a:b;
}

int abs(int a){
    return a>0?a:-a;
}

void lcm_gcd(int a, int b, int* gcd, int* lcm){
    *gcd =1;
    for(int i=2;i<=min(min(a,b),abs(a-b));i++){
        //printf("inside loop\n");
        if(!(a%i || b%i)){
            //printf("inside if %d\n",a%i || b%i);
            *gcd = i;
        }
    }
    *lcm = (a*b)/(*gcd);
}