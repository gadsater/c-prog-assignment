#include<stdio.h>
#include<stdlib.h>
#define N 10

typedef struct {
    int **vectors;
    int count;
}vector_holder;

void vec_init(vector_holder vect,int n){
    vect.vectors = (int **) malloc(n*sizeof(int));
    vect.count = n;
}

int vec_dec_size(vector_holder vect,int k){
    static int count = 1;
    *(vect.vectors + count - 1)=  (int *) malloc(k*sizeof(int));
    count++;
    return count-1;
}

void vec_push(vector_holder vect,int count,int num,int pos){
    *((*vect.vectors+count-1)+pos) = num;
}

int main() {
    vector_holder vec;
    int n=0,k=0,count=0;
    printf("Enter the number of vectors : ");
    scanf("%d",&n);
    vec_init(vec,n);
    printf("Define the vectors :\n");
    for(int i=0;i<n;i++) {
        printf("Enter the size of vector %d: ",i+1);
        scanf("%d",&k);
        count = vec_dec_size(vec,k);
        printf("Enter the elements of vector %d:\n",i+1);
        for(int j=0;j<k;j++){
            printf("Element %d: ",j+1);
            scanf("%d",&n);
            vec_push(vec,count,n,j);
        }
    }
    return 0;
}
