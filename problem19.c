#include<stdio.h>
#include<stdlib.h>
#include<limits.h>

void swap(int *a,int *b){
    int temp;
    temp = *a;
    *a =*b;
    *b = temp;
}

int main() {
    int n;
    printf("Enter the size of array : ");
    scanf("%d",&n);
    int arr[n];
    printf("Enter the integers : ");
    for(int i=0;i<n;i++){
        scanf("%d",&arr[i]);
    }
    
    int *uniqarr = (int *) malloc(sizeof(int));
    int *freqarr = (int *) malloc(sizeof(int));
    int uniqcount = 0;

    for(int i=0;i<n;i++){
        for(int j=i+1;j<n;j++){
            if(arr[i]>arr[j]){
                swap(&arr[i],&arr[j]);
            }
        }
        if(arr[i]!=arr[i-1]){
            uniqcount++;
            if(uniqcount > 1)
            uniqarr = (int *) realloc(uniqarr,uniqcount*sizeof(int));
            uniqarr[uniqcount-1] = arr[i];
            if(uniqcount > 1)
            freqarr = (int *) realloc(freqarr,uniqcount*sizeof(int));
            freqarr[uniqcount-1] = 1;
        }
        if(arr[i]==arr[i-1]){
            freqarr[uniqcount-1]++;
        }
    }

    printf("\n     Element - Frequency\n");
    for(int i=0;i<uniqcount;i++){
        printf("% 12d - %d\n",uniqarr[i],freqarr[i]);
    }

    printf("\nunique elements    : ");
    for(int i=0;i<uniqcount;i++){
        freqarr[i]==1?printf("%d ",uniqarr[i]):0;
    }
    printf("\nduplicate elements : ");
    for(int i=0;i<uniqcount;i++){
        freqarr[i]>1?printf("%d ",uniqarr[i]):0;
    }
    printf("\n");

    return 0;
}