#include<stdio.h>

void swapNumbersCyclic(int *a,int *b,int *c) {
    *b += *a + *c;
    *c = *b - (*a + *c);
    *a = *b - (*a + *c);
    *b = *b - (*a + *c);
}

int main() {
    int a,b,c;
    printf("Enter value of a, b and c respectively: ");
    scanf("%d %d %d",&a,&b,&c);
    printf("Value before swapping: a=%d b=%d c=%d\n",a,b,c);
    swapNumbersCyclic(&a,&b,&c);
    printf("Value after swapping numbers in cycle: a=%d b=%d c=%d\n",a,b,c);
    return 0;
}