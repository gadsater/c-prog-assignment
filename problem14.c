#include<stdio.h>

int main() {
    int m,n;
    printf("Enter the dimensions of the matrix (MxN): ");
    scanf("%d%*c%d",&m,&n);
    int arr[m][n];
    printf("Enter array elements :\n");
    for(int i=0;i<m;i++){
        for(int j=0;j<n;j++){
            scanf("%d",*(arr+i)+j);
        }
    }
    for(int i=1;i<(m*n);i++){
        *(*(arr+(i/n))+i%n) += *(*(arr+(i/n))+i%n-1) ; 
    }
    printf("The sum of the elements is : %d\n",*(*(arr+m-1)+n-1));
    return 0;
}