#include<stdio.h>

int isPerfectNumber(int a){
    int sum = 0;
    for(int i=1;i<a;i++){
        if(a%i==0){
            sum += i;
        }
    }
    return sum == a;
}
int main() {
    int m,n;
    printf("Program to find perfect numbers within a given range :\n");
    printf("Enter the range [a-b] : ");
    scanf("%d%*c%d",&m,&n);
    printf("Perfect numbers are : ");
    for(int i=m;i<=n;i++){
        if(isPerfectNumber(i)) printf("%d ",i);
    }
    printf("\n");
    return 0;
}