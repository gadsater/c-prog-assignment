#include<stdio.h>
#include<stdlib.h>

typedef struct {
    int *vect;
    int k;
}vectorize;

void mod_vect(int i,int j,vectorize *vectors,int m){
    vectors[i-1].vect[j-1] = m;
}

int main() {
    int n;
    printf("Enter the number of vectors to store : ");
    scanf("%d",&n);
    vectorize *vectors = (vectorize *) malloc(n * sizeof(vectorize));
    
    for(int i=0;i<n;i++){
        printf("\nVector %d :\n",i+1);
        printf("Vector size : ");
        scanf("%d",&vectors[i].k);
        vectors[i].vect = (int *) malloc(vectors[i].k * sizeof(int));
        printf("Vector elements : ");
        for(int j=0;j<vectors[i].k;j++){
            scanf("%d",&vectors[i].vect[j]);
        }
    }
    char ans;
    repeat:
    printf("\nWhat would you like to do with the vectors : \n");
    printf("a) Modify the value of jth element of ith vector.\n");
    printf("b) Multiply a specified vector by a scalar value.\n");
    printf("c) Display all vectors.\n");
    printf("Enter choice(q to quit) : ");
    scanf(" %c",&ans);
    
    int i,j,m;

    if(ans != 'q'){
        switch (ans) {
            case 'a': {
                printf("Enter i and j                     : ");
                scanf("%d %d",&i,&j);
                printf("Enter the value to be modified as : ");
                scanf("%d",&m);
                mod_vect(i,j,vectors,m);
                break;
            }
            case 'b': {
                printf("Enter scalar value to multiply : ");
                scanf("%d",&i);
                printf("Enter the nth vector : ");
                scanf("%d",&j);
                for(int p=0;p<vectors[j-1].k;p++){
                    vectors[j-1].vect[p] = i * vectors[j-1].vect[p];
                }
                break;
            }
            case 'c': {
                for(int p=0;p<n;p++){
                    printf("Vector %d : ",p+1);
                    for(int q=0;q<vectors[p].k;q++){
                        printf("%d ",vectors[p].vect[q]);
                    }
                    printf("\n");
                }
                break;
            }
            default : printf("Invalid option!\n");
        }
        goto repeat;
    }

    
    return 0;
}