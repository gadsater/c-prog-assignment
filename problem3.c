#include<stdio.h>

int is_vowel(char c){
    if(c=='a'||c=='e'||c=='i'||c=='o'||c=='u'||c=='A'||c=='E'||c=='I'||c=='O'||c=='U'){
        return 1;
    }
    else return 0;
}

int is_consonant(char c){
    if(is_vowel(c)==0){
        if((c>64 && c<91) || (c>96 && c<123)){
            return 1;
        }
    }
    return 0;
}

int is_whitespace(char c){
    if(c==' '||c=='\t'||c=='\n'||c=='\n'||c=='\v'||c=='\f'||c=='\r'){
        return 1;
    }
    else return 0;
}

int is_digit(char c){
    if(c>47 && c<58){
        return 1;
    }
    else return 0;
}

int main() {
    char str[100];
    int v_count=0,c_count=0,w_count=0,d_count=0,i=0;
    printf("Enter the string: ");
    fgets(str,100,stdin);
    while(*(str+i)){ // if we encounter null character, its ascii value 0 makes the loop terminate. 
        if(is_vowel(*(str+i))) v_count++;
        if(is_consonant(*(str+i))) c_count++;
        if(is_whitespace(*(str+i))) w_count++;
        if(is_digit(*(str+i))) d_count++;
        i++;
    }
    printf("Count of different characters\n\n");
    printf("Vowels :%d\n",v_count);
    printf("Consonants :%d\n",c_count);
    printf("Digits :%d\n",d_count);
    printf("Whitespace :%d\n",w_count);
    return 0;
}