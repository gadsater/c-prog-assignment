#include<stdio.h>
#include<stdlib.h>
#define MAX_STR_SIZE 100

void swap(char *a,char *b){
    char temp;
    temp = *a;
    *a =*b;
    *b = temp;
}

int main() {
    char *str = (char *) malloc(MAX_STR_SIZE * sizeof(char));
    char *uniq_char =(char *)  malloc(sizeof(char)); // array to hold unique characters
    int *count =(int *) malloc(sizeof(int)); // count of each element in array of unique characters

    printf("Enter the string : ");
    fgets(str,MAX_STR_SIZE,stdin);

    int strsize=0; // counts the length of string
    int words=0; // counts the number od words
    int uniqcount=0; // counts the number of unique characters
    int max=0;
    int max_ind=0;

    //printf("Sorted string : ");

    /*  In the loop below, I try to sort array using sequential sort.
        Without knowing the upper bound of array (string length), I
        Iterate the array till it finds null character. Whilst doing
        this, it also does :
            1)  Count the number of elements till null character is
                found.
            2)  Count the no. of words.
            3)  Fill uniq_char array with unique characters.
            4)  Count no.of times an unique element occurs.
    */

    /*
        The sorting is done so that, say I need to find how many no. of
        times a character occurs, picking i'th character, I know that
        0 to i'th character of the array is already sorted, which means
        that any other unique character that is found before 'i' would
        not repeat while further sort is performed.
    */

    for(int i=0;str[i];i++){
        for(int j=i+1;str[j];j++){
            /*printf("i: %d, j: %d, a: %c, b:%c\n", i, j,
            str[i]=='\n'?'!':str[i]==' '?'#':str[i],
            str[j]=='\n'?'!':str[j]==' '?'#':str[j]);*/
            if(str[i]>str[j]){
                swap(&str[i],&str[j]);
            }
        }
        if(str[i]==' ') words++;
        //printf("intermediate string : %s\n",str);
        //printf("%c",str[i]=='\n'?'!':str[i]==' '?'#':str[i]);
        strsize++;
        if(strsize>1){
            if(str[i]!=str[i-1]){
                uniqcount++;
                uniq_char = (char *) realloc(uniq_char,uniqcount*sizeof(char));
                uniq_char[uniqcount-1] = str[i];
                count = (int *) realloc(count,uniqcount*sizeof(int));
                count[uniqcount-1] = 1;
            }
            if(str[i]==str[i-1]){
                count[uniqcount-1]++;
            }
            if(count[uniqcount-1]>max){
              max = count[uniqcount-1];
              max_ind = uniqcount-1;
            }
        }
    }
    strsize--;
    words++;
    printf("Size of string : %d\n",strsize);
    printf("No. of words : %d\n",words);
    printf("Characters present in the string are:\n");
    for(int i=0;i<uniqcount;i++){
      uniq_char[i]==' '?printf("whitespace\n"):printf("%c\n",uniq_char[i]);
    }
    printf("Maximum occuring character in the string is : %c\n",uniq_char[max_ind]);
    /*printf("Unique string   : %s\n",uniq_char);
    printf("Character count : ");
    for(int i=0;i<uniqcount;i++){
        printf("%d",count[i]);
    }
    printf("\n");*/
    return 0;
}
