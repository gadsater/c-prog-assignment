#include<stdio.h>

struct box{
    double length;
    double breadth;
    double height;
    double volume;
}boxes[100]={};

int main() {
    int n;
    printf("Enter no.of boxes (less than 100) : ");
    scanf("%d",&n);

    //code to read dimensions of n boxes
    for(int i=0;i<n;i++){
        printf("\nBox %d: \n",i+1);
        printf("Enter length : ");
        scanf("%lf",&boxes[i].length);
        printf("Enter breadth : ");
        scanf("%lf",&boxes[i].breadth);
        printf("Enter height : ");
        scanf("%lf",&boxes[i].height);
        printf("\n");
    }

    //code to calculate volume of each box
    for(int i=0;i<n;i++){
        boxes[i].volume = boxes[i].length * boxes[i].breadth * boxes[i].height;
    }

    //code to print volume before sorting boxes by volume(ascending)

    printf("\nBefore sorting by volume :\n");
    for(int i=0;i<n;i++){
        printf("\nBox %d: \n",i+1);
        printf("length of box  : %lf\n",boxes[i].length);
        printf("breadth of box : %lf\n",boxes[i].breadth);
        printf("height of box  : %lf\n",boxes[i].height);
        printf("volume of box  : %lf\n",boxes[i].volume);
    }

    //code to sort boxes by volume(ascending)
    struct box temp;

    for (int i=0;i<n-1;i++){
      for(int j=i+1;j<n;j++){
        if(boxes[i].volume>boxes[j].volume){
          temp = boxes[j];
          boxes[j] = boxes[i];
          boxes[i] = temp; 
          /*temp = boxes[j].length;
          boxes[j].length = boxes[j+1].length;
          boxes[j+1].length = temp;

          temp = boxes[j].breadth;
          boxes[j].breadth = boxes[j+1].breadth;
          boxes[j+1]. breadth = temp;
          
          temp = boxes[j].height;
          boxes[j].height = boxes[j+1].height;
          boxes[j+1].height = temp;
          
          temp = boxes[j].volume;
          boxes[j].volume = boxes[j+1].volume;
          boxes[j+1].volume = temp;*/ 
        }
      }
    }

    //code to print volume before sorting boxes by volume(ascending)
    printf("\nAfter sorting by volume :\n");
    for(int i=0;i<n;i++){
        printf("\nBox %d: \n",i+1);
        printf("length of box  : %lf\n",boxes[i].length);
        printf("breadth of box : %lf\n",boxes[i].breadth);
        printf("height of box  : %lf\n",boxes[i].height);
        printf("volume of box  : %lf\n",boxes[i].volume);
    }

    return 0;
}
