#include<stdio.h>
#include<math.h>
int which_quadrant(double x,double y){
    return ((x>0 ? 1 : (x==0 ? 0 : 4)) + (y>0 ? 2 : (y==0 ? 0 : 8))) ;
}

int main() {
    double x,y;
    printf("Enter your coordinates : ");
    scanf("%lf %lf",&x,&y);
    which_quadrant(x,y)%3 ?
    printf("Your point lies on axis/axes\n"):
    printf("Your point lies in quadrant : %d\n",which_quadrant(x,y)/3==3?4:which_quadrant(x,y)/3==4?3:which_quadrant(x,y)/3);
    
    return 0;
}